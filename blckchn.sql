/*
SQLyog Community Edition- MySQL GUI v8.03 
MySQL - 5.6.12-log : Database - blockchain
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`blockchain` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `blockchain`;

/*Table structure for table `candidate` */

DROP TABLE IF EXISTS `candidate`;

CREATE TABLE `candidate` (
  `candidate_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`candidate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `candidate` (
  `candidate_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`candidate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `candidate` */

insert  into `candidate`(`candidate_id`,`student_id`,`post_id`,`date`,`status`) values (1,5,2,'2021-06-05','Approved'),(2,6,1,'2021-06-05','Approved'),(3,7,1,'2021-06-05','Approved');

/*Table structure for table `course` */

DROP TABLE IF EXISTS `course`;

CREATE TABLE `course` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(50) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `course` */

insert  into `course`(`cid`,`cname`,`did`) values (2,'cname22',2),(3,'cname33',2),(4,'crs',2);

/*Table structure for table `department` */

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `department` */

insert  into `department`(`dept_id`,`dept_name`) values (2,'bsc'),(3,'msc');

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `user_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `login` */

insert  into `login`(`login_id`,`username`,`password`,`user_type`) values (1,'a@g.com','a','admin'),(3,'h@g.com','a','staff'),(5,'anu@g.com','a','student'),(6,'anju@g.com','a','student'),(7,'k@g.com','a','student');

/*Table structure for table `post` */

DROP TABLE IF EXISTS `post`;

CREATE TABLE `post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_type` varchar(50) DEFAULT NULL,
  `post_name` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `apply_last_date` date DEFAULT NULL,
  `election_date` date DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `semester` varchar(50) DEFAULT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT 'pending',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `post` */

insert  into `post`(`post_id`,`post_type`,`post_name`,`date`,`apply_last_date`,`election_date`,`dept_id`,`semester`,`batch`,`status`) values (1,'union','pname','2021-06-06','2021-06-07','2021-06-05',3,'1','A','ok');

/*Table structure for table `result` */

DROP TABLE IF EXISTS `result`;

CREATE TABLE `result` (
  `result_id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `no_of_votes` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`result_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `result` */

insert  into `result`(`result_id`,`candidate_id`,`post_id`,`no_of_votes`,`type`) values (3,2,1,2,'Winner'),(4,3,1,1,'Failure');

/*Table structure for table `staff` */

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `photo` varchar(300) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `login_id` int(11) DEFAULT NULL,
  `DOB` varchar(50) DEFAULT NULL,
  `gender` varchar(33) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `staff` */

insert  into `staff`(`sid`,`name`,`photo`,`dept_id`,`email`,`login_id`,`DOB`,`gender`) values (2,'hari','staff/20210605-172951.jpg',2,'h@g.com',3,'2021-06-02','male');

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `photo` varchar(300) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `semester` varchar(50) DEFAULT NULL,
  `batch` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `login_id` int(100) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `student` */

insert  into `student`(`student_id`,`name`,`photo`,`dept_id`,`semester`,`batch`,`email`,`login_id`,`DOB`,`gender`) values (2,'anu','20210605-1731292013-Bullet-500.jpg',2,'1','A','anu@g.com',5,'2021-05-31','female'),(3,'anju','20210605-173158a1.jpg',2,'1','A','anju@g.com',6,'2021-06-08','female'),(4,'keerthana','20210605-173730hai.jpg',2,'1','A','k@g.com',7,'2021-06-01','female');

/*Table structure for table `user_sk55` */

DROP TABLE IF EXISTS `user_sk55`;

CREATE TABLE `user_sk55` (
  `gid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `ky` varchar(300) DEFAULT NULL,
  `sc_ky` varchar(300) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_sk55` */

insert  into `user_sk55`(`gid`,`uid`,`ky`,`sc_ky`,`type`) values (2,5,'[(26539401779456467881446694549523753153528884084511826792885184866518293976979,88069245180516049830058422487751044607390402444548895507651841285599113964799)]','57129330402550671483397876686291408535305408716674697412918799078921158882740L','pending'),(2,6,'[(4255102121713338093721789991468016711618002662645710671506618046840949752032,85563624294600556670321864489603021599451169695583570386645738600311911805609)]','41239632824331519697751377227175513332425025622999050708233106518302068061750L','pending'),(2,7,'[(49421561709532643202651764142902632040223665590888952355996956412068787946358,89513043875167860104535595583644505402841670550632778878401420420355112358039)]','16678434088718493573639238899383474329013532096389577706929715422828240878413L','pending'),(1,5,'[(84303470299841318549338434384671845013042257549862304669537011852343451603036,45043681837770274700728486852037695540144664131473976829672151908827035327677)]','26411106499789434834311486124239922960650423955804858101130707613419487437552L','ok'),(1,6,'[(44490063828794234515468297375961481427096912755859010168391181358386681791018,77357237976630808559833050690911508495610188865467217548305842819662848641172)]','43544941752978799910481238734209308264229036433063724869098836003993421639067L','ok'),(1,7,'[(74565253283209623023007050113211012514461781181004447763309428089279123021273,93375241182586451299344306580806380959010971857385993667434686230065495756684)]','105388386964241408048153781605796569205534552240368553171721808196100069447853L','ok');

/*Table structure for table `vote` */

DROP TABLE IF EXISTS `vote`;

CREATE TABLE `vote` (
  `vid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `candidate_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`vid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `vote` */

insert  into `vote`(`vid`,`uid`,`candidate_id`,`post_id`) values (1,5,2,1),(2,7,2,1),(3,6,3,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
